#include "Reflection.h"

struct MyStruct {
  int e1{6};
  float e2{4.7f};
  double e3{5.4};
  long long e4{1234L};
  std::string e5{"qwerty"};
  char e6{'z'};
  bool e7{true};
};

int main() {
  MyStruct my_struct;

  auto t = reflection::make_reference_tuple(my_struct);

  std::apply([](auto&&... args) { ((std::cout << args << '\n'), ...); }, t);

  std::cout << "\n";

  reflection::for_each(t, [](auto&&... args) { ((std::cout << args << '\n'), ...); });
}
