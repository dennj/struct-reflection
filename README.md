# Struct Reflection

This class allow to return a generic struct elements as a tuple and to iterate through all the struct elements.
This is usefull for struct serializzation.

# How to use

Calling the function **to_tuple()** using a struct as argument will return a tuple with all the struct elements:
>auto  my_tuple  =  reflection::to_tuple(my_struct);

# Build Prerequisites
This code have been tested in Ubuntu 19.04 with boost 1.70.0.

To build protobuf from source, the following tools are needed:

	g++

	cmake

	ninja-build


On Ubuntu/Debian, you can install them with:

	$ sudo apt-get install g++ cmake ninja-build

## Boost
Install Boost following the official guide:
https://www.boost.org/doc/libs/1_70_0/more/getting_started/unix-variants.html

	Download Boost
	
	Extract from the archive
	
	./bootstrap.sh
	
	./b2 install

## Catch2

To run tests:
https://github.com/catchorg/Catch2

```
git clone https://github.com/catchorg/Catch2.git
cd Catch2
cmake -Bbuild -H. -DBUILD_TESTING=OFF
sudo cmake --build build/ --target install
```


# Author
Dennj Osele
dennj.osele@gmail.com

# LICENSE
This code is released with MIT license.

Copyright 2019 Dennj Osele

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
