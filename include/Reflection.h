#pragma once

#include <boost/preprocessor/repetition/repeat.hpp>
#include <deque>
#include <forward_list>
#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <type_traits>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

namespace reflection {

struct any_type { template<class T> constexpr operator T(); };

template<class T, std::size_t... I> constexpr decltype(T{(static_cast<void>(I), std::declval<any_type>())...}, std::true_type{})
test_is_braces_constructible_n(std::index_sequence<I...>);

template <class, class...> std::false_type test_is_braces_constructible_n(...);
template <class T, std::size_t N> using is_braces_constructible_n =
    decltype(test_is_braces_constructible_n<T>(std::make_index_sequence<N>{}));

template<class T, std::size_t L = 0u, std::size_t R = sizeof(T) + 1u>
constexpr std::size_t to_tuple_size_f() {
  constexpr std::size_t M = (L + R) / 2u;

  if constexpr (M == 0)
    return std::is_empty<T>{} ? 0u : throw "Unable to determine number of elements";

  if constexpr (L == M)
    return M;

  if constexpr (is_braces_constructible_n<T, M>{})
    return to_tuple_size_f<T, M, R>();

  return to_tuple_size_f<T, L, M>();
}
template<class T> using to_tuple_size = std::integral_constant<std::size_t, to_tuple_size_f<T>()>;

#ifndef TO_TUPLE_MAX
#define TO_TUPLE_MAX 64
#endif

template<class T>
auto to_tuple_impl(T&&, std::integral_constant<std::size_t, 0>) noexcept {
  return std::forward_as_tuple();
}

#define TO_TUPLE_P(Z,N,_) , p ## N
#define TO_TUPLE_SPECIALIZATION(Z,N,_) \
template<class T> \
auto to_tuple_impl(T&& object, std::integral_constant<std::size_t, N + 1>) noexcept { \
        auto&& [p BOOST_PP_REPEAT_ ## Z(N, TO_TUPLE_P, nil)] = object; \
        return std::forward_as_tuple(p BOOST_PP_REPEAT_ ## Z(N, TO_TUPLE_P, nil)); \
    }
BOOST_PP_REPEAT(TO_TUPLE_MAX, TO_TUPLE_SPECIALIZATION, nil)
#undef TO_TUPLE_SPECIALIZATION
#undef TO_TUPLE_P

template<class T, class = struct current_value, std::size_t = TO_TUPLE_MAX, class = struct required_value, std::size_t N>
auto to_tuple_impl(T&&, std::integral_constant<std::size_t, N>) noexcept {
  static_assert(N <= TO_TUPLE_MAX, "TO_TUPLE_MAX too small");
}

template<class T>//TODO , class = std::enable_if_t<std::is_class<T>::value && std::is_standard_layout<T>::value>>
auto make_reference_tuple(T&& object) noexcept {
  return to_tuple_impl(std::forward<T>(object), to_tuple_size<std::decay_t<T>>{});
}

//Tuple for each
template<std::size_t I = 0, typename FuncT, typename... Tp>
inline typename std::enable_if_t<I == sizeof...(Tp), void>
for_each(std::tuple<Tp...> &, FuncT) noexcept // Unused arguments are given no names.
{ }

template<std::size_t I = 0, typename FuncT, typename... Tp>
inline typename std::enable_if_t<I < sizeof...(Tp), void>
for_each(std::tuple<Tp...>& t, FuncT f) noexcept
{
  f(std::get<I>(t));
  for_each<I + 1, FuncT, Tp...>(t, f);
}

//specialize a type for all of the STL containers.
namespace is_stl_container_impl{
template <typename T>       struct is_stl_container:std::false_type{};
template <typename T, std::size_t N> struct is_stl_container<std::array    <T,N>>    :std::true_type{};
template <typename... Args> struct is_stl_container<std::vector            <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::deque             <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::list              <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::forward_list      <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::set               <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::multiset          <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::map               <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::multimap          <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::unordered_set     <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::unordered_multiset<Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::unordered_map     <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::unordered_multimap<Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::stack             <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::queue             <Args...>>:std::true_type{};
template <typename... Args> struct is_stl_container<std::priority_queue    <Args...>>:std::true_type{};
}

//type trait to utilize the implementation type traits as well as decay the type
template <typename T> struct is_stl_container {
  static constexpr bool const value = is_stl_container_impl::is_stl_container<std::decay_t<T>>::value;
};

//Forward declaration
template<class S1, class S2>
inline bool compare_structs(S1 s1, S2 s2) noexcept;

// Compare two tuple

// End of the tuple reaced
template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if_t<I == sizeof...(Tp), bool>
compare_tuples(std::tuple<Tp...> &, std::tuple<Tp...> &) noexcept
{ return true; }

//Iterate the two tuples
template<std::size_t I = 0, typename... Tp>
inline typename std::enable_if_t<I < sizeof...(Tp),  bool>
compare_tuples(std::tuple<Tp...>& t1, std::tuple<Tp...>& t2) noexcept
{
  auto e1 = std::get<I>(t1);
  auto e2 = std::get<I>(t2);

  if constexpr (std::is_fundamental_v<decltype(e1)> || is_stl_container<decltype(e1)>::value)
  {
    if constexpr (std::is_floating_point_v<typename std::remove_reference_t< decltype(e1) >>) // Compare float with epsilon to avoid numerical instability
    {
      if(std::abs(e1 -e2) >= std::numeric_limits<decltype(e1)>::epsilon())
        return false;
    }
    else
    {
      if (e1 != e2) // Normal compare for everithing else
        return false;
    }
  }
  else
  {
    if constexpr (std::is_class_v<decltype (e1)>)
    {
      if(!reflection::compare_structs(e1, e2))
        return false;
    }

    static_assert(std::is_class_v<decltype (e1)>, "Unexpected case" );
  }

  return compare_tuples<I + 1, Tp...>(t1, t2); // Compare next elements in tuples
}

template<typename... Tp1, typename... Tp2> // Check if the the touples are of the same type
inline bool compare_tuples(std::tuple<Tp1...>& t1, std::tuple<Tp2...>& t2) noexcept
{
  if constexpr (!std::is_same_v<Tp1...,Tp2...>)
    return false;
  else {
    return compare_tuples<0, Tp1...>(t1, t2);
  }
}

// Compare structs
template<class S1, class S2>
inline bool compare_structs(S1 s1, S2 s2) noexcept
{
  auto t1 = reflection::make_reference_tuple(s1);
  auto t2 = reflection::make_reference_tuple(s2);

  return compare_tuples(t1, t2);
}

}
