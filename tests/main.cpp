#define CATCH_CONFIG_MAIN

#include "pch.h"
#include "Reflection.h"

namespace UnitTestRef {

TEST_CASE("Struct_1_test")
{
  SECTION("Struct to tuple")
  {
    struct MyStruct {
      int e1{6};
      float e2{4.7f};
      double e3{5.4};
      long long e4{1234L};
      char e5{'z'};
      bool e6{true};
      std::string e7{"qwerty"};
    };

    MyStruct my_struct;

    auto t = reflection::make_reference_tuple(my_struct);

    REQUIRE(std::tuple_size<decltype(t)>::value == 7);
    REQUIRE(std::get<0>(t) == 6);
    REQUIRE(std::get<1>(t) == 4.7f);
    REQUIRE(std::get<2>(t) == 5.4);
    REQUIRE(std::get<3>(t) == 1234L);
    REQUIRE(std::get<4>(t) == 'z');
    REQUIRE(std::get<5>(t) == true);
    REQUIRE(std::get<6>(t) == "qwerty");
    REQUIRE_FALSE(std::get<6>(t) == "abcd");
  }

  SECTION("Data manipulation tuple to struct")
  {
    struct MyStruct {
      int e1{6};
      float e2{4.7f};
    };

    MyStruct my_struct;

    auto t = reflection::make_reference_tuple(my_struct);

    REQUIRE(std::get<0>(t) == my_struct.e1);
    REQUIRE(std::get<1>(t) == my_struct.e2);

    t = std::make_tuple(3, 3);

    REQUIRE(std::get<0>(t) == my_struct.e1);
    REQUIRE(std::get<1>(t) == my_struct.e2);
  }

  SECTION("Numerical Stability +/-0")
  {
    struct {
      float e{0.0f};
    } s1;

    struct {
      float e{-0.0f};
    } s2;

    REQUIRE(reflection::compare_structs(s1, s2));

    s1.e = 1.0f / s1.e; // +Inf
    s2.e = 1.0f / s2.e; // -Inf

    REQUIRE_FALSE(reflection::compare_structs(s1, s2));

    s2.e = -s2.e;

    REQUIRE(reflection::compare_structs(s1, s2));
  }

  SECTION("Numerical Stability")
  {
    struct {
      float e{0.3f + 0.4f};
      int e2{4};
    } s1;

    struct {
      float e{0.7f};
      int e2{4};
    } s2;

    REQUIRE(reflection::compare_structs(s1, s2));
  }

  SECTION("Compare different structs")
  {
    struct {
      float e{ 0.0f };
    } s1;

    struct {
      int e{ 0 };
    } s2;

    REQUIRE_FALSE(reflection::compare_structs(s1, s2));
  }

  SECTION("Compare different tuple")
  {
    struct {
      float e{ 0.0f };
    } s1;

    struct {
      int e{ 0 };
    } s2;

    auto t1 = reflection::make_reference_tuple(s1);
    auto t2 = reflection::make_reference_tuple(s2);

    REQUIRE_FALSE(reflection::compare_tuples(t1, t2));
  }

  SECTION("Compare tuples")
  {
    struct {
      float e1{0.3f + 0.4f};
      int e2{4};
      double e3 {3.7};
      long double e4 {2.2L};
    } s1;

    struct {
      float e1 {0.7f};
      int e2 {4};
      double e3 {3.7};
      long double e4 {2.2L};
    } s2;

    auto t1 = reflection::make_reference_tuple(s1);
    auto t2 = reflection::make_reference_tuple(s2);

    REQUIRE(reflection::compare_tuples(t1, t2));

    s2.e4 = 2.1L;

    REQUIRE_FALSE(reflection::compare_tuples(t1, t2));
  }

  SECTION("Struct with array")
  {
    struct MyStructArray {
      int e1{6};
      std::array<float,3> e2{4.7f, 8.5f, 9.7f};
    };

    MyStructArray s1, s2;

    auto t1 = reflection::make_reference_tuple(s1);
    auto t2 = reflection::make_reference_tuple(s2);

    REQUIRE(reflection::compare_tuples(t1, t2));

    s2.e2.at(2) = 2.1f;

    REQUIRE_FALSE(reflection::compare_tuples(t1, t2));

    s2.e2.at(2) = 9.7f;

    REQUIRE(reflection::compare_tuples(t1, t2));
  }

  SECTION("Struct with vector")
  {
    struct MyStruct {
      int e1{6};
      std::vector<float> e2{4.7f, 8.5f, 9.7f};
    };

    MyStruct s1, s2;

    REQUIRE(reflection::compare_structs(s1, s2));

    s2.e2.pop_back();

    REQUIRE_FALSE(reflection::compare_structs(s1, s2));

    s2.e2.push_back(9.7f);

    REQUIRE(reflection::compare_structs(s1, s2));
  }

  SECTION("Struct with sub-struct")
  {
    struct SubStruct {
      int e1 {0};
    };

    struct MyStruct {
      int e1{6};
      SubStruct e2 {1};
    };

    MyStruct s1, s2;

    REQUIRE(reflection::compare_structs(s1, s2));

    s2.e2.e1 = 2;

    REQUIRE_FALSE(reflection::compare_structs(s1, s2));

    s1.e2.e1 = 2;

    REQUIRE(reflection::compare_structs(s1, s2));
  }

  SECTION("Struct with sub-class")
  {
    struct SubStruct1 {
      float e1 {5.4f};
    };

    class SubStruct2 {
     public:
      int e1 {0};
      SubStruct1 e2;
    };

    struct MyStruct {
      int e1{6};
      SubStruct2 e2;
    };

    MyStruct s1, s2;

    REQUIRE(reflection::compare_structs(s1, s2));

    s1.e2.e2.e1 = 5.3f;

    REQUIRE_FALSE(reflection::compare_structs(s1, s2));

    s1.e2.e2.e1 = 5.4f;

    REQUIRE(reflection::compare_structs(s1, s2));
  }
}

}  // namespace UnitTestRef
